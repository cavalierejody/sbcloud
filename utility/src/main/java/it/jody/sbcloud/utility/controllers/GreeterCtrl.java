package it.jody.sbcloud.utility.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Jody on 19/03/2018.
 */
@RestController
public class GreeterCtrl {

    @GetMapping("/greet")
    public String greet() {
        return "hello";
    }
}
